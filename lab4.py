# -*- coding: utf-8 -*-
import sys

import numpy
from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg
from matplotlib.figure import Figure

import odesolve
from approx import *

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class MainWindow:
    def __init__(self):
        app = QApplication(sys.argv)
        self.window = QMainWindow()
        self.setup_ui(self.window)
        self.window.show()
        sys.exit(app.exec_())

    def setup_ui(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.setWindowModality(QtCore.Qt.NonModal)
        MainWindow.resize(738, 475)
        MainWindow.setMinimumSize(QtCore.QSize(600, 400))
        MainWindow.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_3.setSizeConstraint(QtGui.QLayout.SetNoConstraint)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.frame_side = QtGui.QFrame(self.centralwidget)
        self.frame_side.setMinimumSize(QtCore.QSize(220, 0))
        self.frame_side.setMaximumSize(QtCore.QSize(240, 16777215))
        self.frame_side.setFrameShape(QtGui.QFrame.Panel)
        self.frame_side.setFrameShadow(QtGui.QFrame.Sunken)
        self.frame_side.setLineWidth(0)
        self.frame_side.setObjectName(_fromUtf8("frame_side"))
        self.verticalLayout = QtGui.QVBoxLayout(self.frame_side)
        self.verticalLayout.setMargin(5)
        self.verticalLayout.setSpacing(2)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.frame_content = QtGui.QFrame(self.frame_side)
        self.frame_content.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_content.setFrameShadow(QtGui.QFrame.Plain)
        self.frame_content.setObjectName(_fromUtf8("frame_content"))
        self.formLayout = QtGui.QFormLayout(self.frame_content)
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setMargin(0)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.frame_ode = QtGui.QFrame(self.frame_content)
        self.frame_ode.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_ode.setFrameShadow(QtGui.QFrame.Plain)
        self.frame_ode.setLineWidth(0)
        self.frame_ode.setObjectName(_fromUtf8("frame_ode"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.frame_ode)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 20)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.label_ode_header = QtGui.QLabel(self.frame_ode)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_ode_header.setFont(font)
        self.label_ode_header.setObjectName(_fromUtf8("label_ode_header"))
        self.verticalLayout_3.addWidget(self.label_ode_header)
        self.label_ode_cont = QtGui.QLabel(self.frame_ode)
        self.label_ode_cont.setObjectName(_fromUtf8("label_ode_cont"))
        self.verticalLayout_3.addWidget(self.label_ode_cont)
        self.formLayout.setWidget(0, QtGui.QFormLayout.SpanningRole, self.frame_ode)
        self.label_init = QtGui.QLabel(self.frame_content)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_init.setFont(font)
        self.label_init.setObjectName(_fromUtf8("label_init"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_init)
        self.frame_init_cond = QtGui.QFrame(self.frame_content)
        self.frame_init_cond.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_init_cond.setFrameShadow(QtGui.QFrame.Plain)
        self.frame_init_cond.setObjectName(_fromUtf8("frame_init_cond"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.frame_init_cond)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 20)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_y = QtGui.QLabel(self.frame_init_cond)
        self.label_y.setObjectName(_fromUtf8("label_y"))
        self.horizontalLayout.addWidget(self.label_y)
        self.lineEdit_x0 = QtGui.QLineEdit(self.frame_init_cond)
        self.lineEdit_x0.setValidator(QDoubleValidator())
        self.lineEdit_x0.setObjectName(_fromUtf8("lineEdit_x0"))
        self.horizontalLayout.addWidget(self.lineEdit_x0)
        self.label_eq = QtGui.QLabel(self.frame_init_cond)
        self.label_eq.setObjectName(_fromUtf8("label_eq"))
        self.horizontalLayout.addWidget(self.label_eq)
        self.lineEdit_y0 = QtGui.QLineEdit(self.frame_init_cond)
        self.lineEdit_y0.setValidator(QDoubleValidator())
        self.lineEdit_y0.setObjectName(_fromUtf8("lineEdit_y0"))
        self.horizontalLayout.addWidget(self.lineEdit_y0)
        self.formLayout.setWidget(2, QtGui.QFormLayout.SpanningRole, self.frame_init_cond)
        self.label_end_segment = QtGui.QLabel(self.frame_content)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_end_segment.setFont(font)
        self.label_end_segment.setObjectName(_fromUtf8("label_end_segment"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.label_end_segment)
        self.lineEdit_end_segment = QtGui.QLineEdit(self.frame_content)
        self.lineEdit_end_segment.setValidator(QDoubleValidator())
        self.lineEdit_end_segment.setObjectName(_fromUtf8("lineEdit_end_segment"))
        self.formLayout.setWidget(4, QtGui.QFormLayout.SpanningRole, self.lineEdit_end_segment)
        self.label_accuracy = QtGui.QLabel(self.frame_content)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_accuracy.setFont(font)
        self.label_accuracy.setObjectName(_fromUtf8("label_accuracy"))
        self.formLayout.setWidget(5, QtGui.QFormLayout.LabelRole, self.label_accuracy)
        self.lineEdit_accuracy = QtGui.QLineEdit(self.frame_content)
        self.lineEdit_accuracy.setValidator(QDoubleValidator())
        self.lineEdit_accuracy.setObjectName(_fromUtf8("lineEdit_accuracy"))
        self.formLayout.setWidget(6, QtGui.QFormLayout.SpanningRole, self.lineEdit_accuracy)
        self.verticalLayout.addWidget(self.frame_content)
        self.button_solve = QtGui.QPushButton(self.frame_side)
        self.button_solve.setObjectName(_fromUtf8("button_solve"))
        self.verticalLayout.addWidget(self.button_solve)
        self.horizontalLayout_3.addWidget(self.frame_side)
        self.figure = Figure(frameon=False)
        self.canvas = FigureCanvasQTAgg(self.figure)
        ax = self.figure.add_subplot(111)
        ax.grid()
        self.horizontalLayout_3.addWidget(self.canvas)
        MainWindow.setCentralWidget(self.centralwidget)

        MainWindow.setWindowTitle(_translate("MainWindow", "ODE Solver", None))
        self.label_ode_header.setText(_translate("MainWindow", "Ordinary differential equation:", None))
        self.label_ode_cont.setText(_translate("MainWindow", "y\' = x^2 + 4y - x", None))
        self.label_init.setText(_translate("MainWindow", "Initial condition:", None))
        self.label_y.setText(_translate("MainWindow", "y (", None))
        self.label_eq.setText(_translate("MainWindow", ") = ", None))
        self.label_end_segment.setText(_translate("MainWindow", "End of segment:", None))
        self.label_accuracy.setText(_translate("MainWindow", "Accuracy:", None))
        self.button_solve.setText(_translate("MainWindow", "Solve", None))
        self.bind_actions()
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def bind_actions(self):
        self.button_solve.clicked.connect(self.solve)

    def solve(self):
        if (self.lineEdit_x0.text() + " ").isspace():
            self.show_error_dialog("Please enter the x0")
            return
        if (self.lineEdit_y0.text() + " ").isspace():
            self.show_error_dialog("Please enter the y0")
            return
        if (self.lineEdit_accuracy.text() + " ").isspace():
            self.show_error_dialog("Please enter the accuracy")
            return
        if (self.lineEdit_end_segment.text() + " ").isspace():
            self.show_error_dialog("Please enter the end of the segment")
            return

        accuracy = float(self.lineEdit_accuracy.text())
        x0 = float(self.lineEdit_x0.text())
        y0 = float(self.lineEdit_y0.text())
        end_segment = float(self.lineEdit_end_segment.text())

        if not 0 < accuracy <= 1:
            self.show_error_dialog("Accuracy should be in the interval (0; 1]")
            return
        if end_segment <= x0:
            self.show_error_dialog("The end of segment must be greater than x0")
            return

        print("x0 = " + str(x0) + " y0 = " + str(y0) + " accuracy = " + str(accuracy) + " end_segment = " + str(
            end_segment))

        def f(x, y):
            return x ** 2 + 4 * y - x

        xs, ys = odesolve.solve_ode(f, x0, y0, end_segment, accuracy)
        self.do_approximate(xs, ys, ApproximatingFunction.QUADRATIC)

    def do_approximate(self, xs, ys, approx_func_type):
        if len(set(xs)) != len(xs):
            self.show_error_dialog("Table contains two identical x")
            return
        if approx_func_type is ApproximatingFunction.EXPONENTIAL and sorted(ys)[0] <= 0.0:
            self.show_error_dialog("For an exponential approximating function all y's must be positive")

        print("X values: " + str(xs))
        print("Y values: " + str(ys))
        print("Approximating function: " + str(approx_func_type))

        # print points
        diff = max([xs[i + 1] - xs[i] for i in range(len(xs) - 1)]) / 10.0
        x = numpy.linspace(min(xs) - diff, max(xs) + diff, 100)
        ax = self.figure.get_axes()[0]
        ax.clear()
        ax.grid()
        ax.plot(xs, ys, 'o')

        approx = Approximate(xs, ys)
        coefficients1, func1 = approx.approximate(approx_func_type)

        # plot approx function
        line1, = ax.plot(x, func1(x), label="1: " + self.function_to_str(approx_func_type, coefficients1))
        # print label
        # ax.add_artist(ax.legend(handles=[line1], loc=2))
        self.canvas.draw()

    @staticmethod
    def function_to_str(approx_func_type, coefficients):
        s = 'y = '
        if approx_func_type is ApproximatingFunction.LINEAR:
            s += '{}x{:+}'.format(coefficients[0], coefficients[1])
        elif approx_func_type is ApproximatingFunction.QUADRATIC:
            s += '{}x^2 {:+}x {:+}'.format(coefficients[0], coefficients[1], coefficients[2])
        elif approx_func_type is ApproximatingFunction.EXPONENTIAL:
            s += '{} e^({}x)'.format(coefficients[0], coefficients[1])
        else:
            return ""
        return s

    def show_error_dialog(self, error_text):
        msg = QMessageBox(self.window)
        msg.setIcon(QMessageBox.Critical)
        msg.setText(error_text)
        msg.setWindowTitle("Error")
        msg.exec_()


if __name__ == "__main__":
    window = MainWindow()
