import math
from enum import Enum

from numpy import linalg, zeros


class ApproximatingFunction(Enum):
    LINEAR = "Linear"
    QUADRATIC = "Quadratic"
    EXPONENTIAL = "Exponential"


class Approximate:
    def __init__(self, xs, ys):
        self.xs = xs
        self.ys = ys

    def approximate(self, approx_func_type):
        if approx_func_type is ApproximatingFunction.LINEAR:
            return self.approximate_linear()
        elif approx_func_type is ApproximatingFunction.QUADRATIC:
            return self.approximate_quadratic()
        elif approx_func_type is ApproximatingFunction.EXPONENTIAL:
            return self.approximate_exponential()
        else:
            raise ValueError("Bad approximating function type")

    def approximate_linear(self):
        matrix_a = [[sum(map(lambda x: x ** 2, self.xs)), sum(self.xs)],
                    [sum(self.xs), len(self.xs)]]
        matrix_b = [sum(map(lambda x, y: x * y, self.xs, self.ys)), sum(self.ys)]
        coefficients = linalg.solve(matrix_a, matrix_b)

        def func(x): return coefficients[0] * x + coefficients[1]

        return coefficients, func

    def approximate_exponential(self):
        # to linear form
        ln_ys = list(map(lambda y: math.log(y), self.ys))
        saved_ys = self.ys
        self.ys = ln_ys
        coefficients, f = self.approximate_linear()
        self.ys = saved_ys
        # to exponential form
        coefficients = coefficients[::-1]
        coefficients[0] = math.e ** coefficients[0]

        def func(x): return coefficients[0] * math.e ** (coefficients[1] * x)

        return coefficients, func

    def approximate_quadratic(self):
        matrix_a = zeros((3, 3))
        matrix_b = [0] * 3

        matrix_a[0][0] = sum(map(lambda x: x ** 4, self.xs))
        matrix_a[0][1] = sum(map(lambda x: x ** 3, self.xs))
        matrix_a[0][2] = sum(map(lambda x: x ** 2, self.xs))
        matrix_a[1][0] = matrix_a[0][1]
        matrix_a[1][1] = matrix_a[0][2]
        matrix_a[1][2] = sum(self.xs)
        matrix_a[2][0] = matrix_a[0][2]
        matrix_a[2][1] = matrix_a[1][2]
        matrix_a[2][2] = len(self.xs)

        matrix_b[0] = sum(map(lambda x, y: x ** 2 * y, self.xs, self.ys))
        matrix_b[1] = sum(map(lambda x, y: x * y, self.xs, self.ys))
        matrix_b[2] = sum(self.ys)

        print(matrix_a)
        print(matrix_b)
        print(self.xs)
        print(self.ys)

        coefficients = linalg.solve(matrix_a, matrix_b)

        def func(x): return coefficients[0] * x ** 2 + coefficients[1] * x + coefficients[2]

        return coefficients, func

    def remove_one_point(self, func):
        if len(self.xs) < 3:
            return
        max_diff = 0
        max_diff_i = -1
        for i in range(len(self.xs)):
            cur_diff = (self.ys[i] - func(self.xs[i])) ** 2
            if cur_diff > max_diff:
                max_diff = cur_diff
                max_diff_i = i

        if max_diff_i < 0:
            return
        point = [self.xs[max_diff_i], self.ys[max_diff_i]]
        del self.xs[max_diff_i]
        del self.ys[max_diff_i]
        return point
