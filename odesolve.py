from math import ceil

import numpy


def solve_ode(f, x0, y0, end_segment, accuracy):
    h = pow(accuracy, 1 / 3)
    num = max(int(ceil((end_segment - x0) / h)) + 1, 4)
    h = (end_segment - x0) / (num - 1)

    xs = numpy.linspace(x0, end_segment, num)
    ys = [0.0] * len(xs)
    ys[0] = y0

    runge_kutta(xs, ys, f, h, 3)
    adams4(xs, ys, f, h)
    return xs, ys


def runge_kutta(xs, ys, f, h, len):
    for i in range(0, len):
        k1 = f(xs[i], ys[i])
        k2 = f(xs[i] + h / 2, ys[i] + h * k1 / 2)
        k3 = f(xs[i] + h / 2, ys[i] + h * k2 / 2)
        k4 = f(xs[i] + h, ys[i] + h * k3)
        ys[i + 1] = ys[i] + (h / 6) * (k1 + 2 * k2 + 2 * k3 + k4)


def adams4(xs, ys, f, h):
    for i in range(3, len(xs) - 1):
        fs = [0] * 4
        for j in range(0, -4, -1):
            fs[j] = f(xs[i + j], ys[i + j])
        ys[i + 1] = ys[i] + h * (55 * fs[0] - 59 * fs[-1] + 37 * fs[-2] - 9 * fs[-3]) / 24
